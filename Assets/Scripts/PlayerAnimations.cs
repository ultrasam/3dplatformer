﻿// Este script controla las animaciones del personaje Jugador. Normalmente, la mayoría
// de las veces esta funcionalidad se agregaría al script PlayerMovement en lugar de tener
// su propio script, ya que sería más eficiente. Sin embargo, para cumplir con los principios,
// SOLID, se ha separado esta funcionalidad.

using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
	MovimientoJugador _movement;	//Referencia al Script MovimientoJugador
	Rigidbody _rigidBody;		//Referencia al componente Rigidbody
	PlayerInputControls _input;	//Referencia al Script PlayerInputControls
	Animator _anim;				//Referencia al componente Animator

	int _horizontalSpeedParamID;	//ID del parámetro horizontalSpeed
	int _groundParamID;				//ID del parámetro isOnGround
	int _crouchParamID;				//ID del parámetro isCrouching
	int _walkingCarefullyParamID;   //ID del parámetro isWalkingCarefully
	
	void Start()
	{
		//Obtener los id de los parámetros del Animator. Esto es mucho más eficiente
		//que pasar strings al Animator
		_horizontalSpeedParamID = Animator.StringToHash("horizontalSpeed");
		_groundParamID = Animator.StringToHash("isOnGround");
		_crouchParamID = Animator.StringToHash("isCrouching");
		_walkingCarefullyParamID = Animator.StringToHash("isWalkingCarefully");
	
		//Obtener referencias a los componentes necesarios
		_movement	= GetComponent<MovimientoJugador>();
		_rigidBody	= GetComponent<Rigidbody>();
		_input		= GetComponent<PlayerInputControls>();
		_anim		= GetComponent<Animator>();
		
		//Si alguno de los componentes no existe...
		if(_movement == null || _rigidBody == null || _input == null || _anim == null)
		{
			//...Reportar error y destruir este componente
			Debug.LogError("Un componente necesario no se encuentra en el Personaje del Jugador");
			Destroy(this);
		}
	}

	void Update()
	{
		//Actualizar el Animator con los valores apropiados
		_anim.SetBool(_groundParamID, _movement.isOnGround);
		_anim.SetBool(_crouchParamID, _movement.isCrouching);
		_anim.SetBool(_walkingCarefullyParamID, _movement.isWalkingCarefully);

		//Use the absolute value of speed so that we only pass in positive numbers
		_anim.SetFloat(_horizontalSpeedParamID, Mathf.Abs(_rigidBody.velocity.x));
	}

	//This method is called from events in the animation itself. This keeps the footstep
	//sounds in sync with the visuals
	public void StepAudio()
	{
		//Tell the Audio Manager to play a footstep sound
		//AudioManager.PlayFootstepAudio();
	}

	//This method is called from events in the animation itself. This keeps the footstep
	//sounds in sync with the visuals
	public void CrouchStepAudio()
	{
		//Tell the Audio Manager to play a crouching footstep sound
		//AudioManager.PlayCrouchFootstepAudio();
	}
}
