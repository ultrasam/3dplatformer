﻿// Este script controla el movimiento y las físicas del jugador dentro del juego
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class MovimientoJugador : MonoBehaviour
{
    public bool drawDebugRaycasts = true;	//Se deben visualizar los chequeos Ambientales?

    [Header("Movement Properties")]
    #region Propiedades del Movimiento
    public float acceleration = 8f;			//Aceleración del jugador
    public float maxHorSpeed = 8f;			//Velocidad horizontal máxima del jugador
    public float coyoteDuration = .05f;		//Cuanto tiempo despues de caer puede saltar el jugador
    public float maxFallSpeed = -25f;		//Max speed player can fall
    public float turningSpeed = 5;
    public float crouchSpeedDivisor = 5;			//Factor de reducción de velocidad mientras esté agachado
    public float carefulWalkingSpeedDivisor = 19;	//Factor de reducción de velocidad mientras esté caminando con cuidado    #endregion
    #endregion
    
    [Header("Jump Properties")]
    #region Propiedades del Salto
    public float jumpForce = 5f;			//Fuerza inicial del salto
    public float jumpHoldForce = .25f;		//Fuerza incremental cuando se mantiene presionado el salto
    public float jumpHoldDuration = .2f;	//Por cuanto tiempo se puede mantener presionado el salto
    public float crouchJumpBoost = 2.5f;	//Extra fuerza de salto al estar agachado
    #endregion
    
    [Header("Environment Check Properties")]
    #region Propiedades de Chequeo Ambiental
    public float footXOffset = .3f;			//X Offset del raycast de los pies
    public float footYOffset = .1f;			//Y Offset del raycast de los pies
    public float groundDistance = .2f;		//Distancia a la que se considera que el jugador está en el suelo
    public float holguraCabeza = .5f;		//Espacio necesario por encima de la cabeza del jugador
    public LayerMask groundLayer;			//Capa del suelo
    #endregion

    [Header ("Status Flags")]
    #region Inicadores de Status
    public bool isOnGround;          //Está el jugador en el suelo?
    public bool isJumping;           //Está el jugador saltando?
    public bool isTurning = false;   //Está el jugador rotando?
    public bool isCrouching = false; //Está el jugador agachado?
    public bool isWalkingCarefully;  //Está el jugador caminando con cuidado?
    public bool isHeadBlocked;       //Está la cabeza del jugador bloqueada
    #endregion
    
    private PlayerInputControls _input;		//El input actual del jugador
    private CapsuleCollider _bodyCollider;	//El componente collider
    private Rigidbody _myRb;				//El componente rigidbody

    #region Propiedades Adicionales
    private float _jumpTime;				//Variable to hold jump duration
    private float _coyoteTime;				//Variable to hold coyote duration
    
    private int _facingDirection = 1;		//Dirección a la cual está viendo el jugador
    private float _turningSpeedFactor = 0;
    private readonly Vector3[] _axes = { Vector3.up, Vector3.down };
    private Vector3 _axis = Vector3.up;
    private float _colliderStandHeight;		//Altura del collider al estar de pie
    private Vector3 _colliderStandCenter;	//Punto Central del collider al estar de pie
    private float _colliderCrouchHeight;	//Altura del collider al estar agachado
    private Vector3 _colliderCrouchCenter;	//Punto Central del collider al estar agachado
    private float _originalGroundDistance;	//Valor original de la distancia a la que se considera cerca del suelo
    #endregion
    
    // Start is called before the first frame update
    void Start()
    {
        //Obtener referencia a los componentes requeridos
        _input = GetComponent<PlayerInputControls>();
        _myRb = GetComponent<Rigidbody>();
        _bodyCollider = GetComponent<CapsuleCollider>();
                
        //Obtener la altura y el punto central inicial del collider
        _colliderStandHeight = _bodyCollider.height;
        _colliderStandCenter = _bodyCollider.center;

        //Calcular la altura y el punto central del collider mientras está agachado
        _colliderCrouchHeight = _colliderStandHeight/1.5f;
        _colliderCrouchCenter = new Vector3(_colliderStandCenter.x, _colliderStandCenter.y / 1.5f, _colliderStandCenter.z);
                
        _originalGroundDistance = groundDistance;
    }

    private void FixedUpdate()
    {
	    //Chequear el ambiente para determinar estatus
	    PhysicsCheck();

	    //Procesar movimientos en tierra y aire
	    GroundMovement();		
	    MidAirMovement();
    }

	void PhysicsCheck()
	{
		//Se inicia asumiendo que el jugador no está en el suelo y que la cabeza no está bloqueada
		isOnGround = false;
		isHeadBlocked = false;

		//RayCasts para el pie izquierdo y derecho
		bool leftCheck = Raycast(new Vector2(-footXOffset, footYOffset), Vector2.down, groundDistance);
		bool rightCheck = Raycast(new Vector2(footXOffset, footYOffset), Vector2.down, groundDistance);

		//Si cualquiera de los dos rays toca el suelo, el jugador está en el suelo...
		if (leftCheck || rightCheck)
			isOnGround = true;
		else //... de lo contrario no está en el suelo
			isOnGround = false;
		
		//RayCasts para chequear encima de la cabeza del jugador
		var headCheck = Raycast(new Vector2(0f, _bodyCollider.height), Vector2.up, holguraCabeza);

		//If that ray hits, the player's head is blocked
		if (headCheck)
			isHeadBlocked = true;
	}

	void GroundMovement()
	{
		//Manejar el input de agacharse.
		//Si se mantiene presionado el botón de agacharse pero aún no está agachado, entonces agacharse
		if (_input.crouchHeld && !isCrouching && isOnGround)
			Crouch();
		//De lo contrario, si no se está presionando el botón de agacharse actualmente, levantarse
		else if (!_input.crouchHeld && isCrouching)
			StandUp();
		//De lo contrario, si está agachado y ya no está en el piso, levantarse
		else if (!isOnGround && isCrouching)
			StandUp();

		//Manejar el input de Acción Especial mientras está en el suelo y caminando
		//Si se mantiene presionado el botón de acción especial pero aún no está caminando cuidadosamente
		//y se encuentra en tierra con una velocidad de cercana a cero, entonces caminar cuidadosamente
		if (_input.specialActionHeld && !isWalkingCarefully && isOnGround && !isCrouching && Mathf.Abs(_myRb.velocity.x) < 1)
			WalkCarefully();
		//De lo contrario, si no se está presionando el botón de acción especial actualmente, detener la acción especial
		else if (!_input.specialActionHeld && isWalkingCarefully)
			StopWalkingCarefully();
		//De lo contrario, si está caminando cuidadosamente y ya no está en el piso, detener la acción especial
		else if (!isOnGround && isWalkingCarefully)
			StopWalkingCarefully();
		
		//Calcular la aceleracion horizontal deseada basada en los inputs
		float xAccel = acceleration * _input.horizontal;

		//Si el signo de la aceleracion horizontal y la dirección no son iguales, voltear al personaje
		if (xAccel * _facingDirection < 0f)
			RotateCharacter();

		//Aplicar la aceleración deseada sólo si está en el suelo
		if(isOnGround)
			_myRb.AddForce(Vector3.right * xAccel);
		
		//Si el jugador está agachado, reducir la velocidad horizontal máxima
		float limiteVelHorizontal = maxHorSpeed;
		if (isCrouching)
			limiteVelHorizontal /= crouchSpeedDivisor;
		//Si el jugador está caminando cuidadosamente, reducir la velocidad horizontal máxima
		if (isWalkingCarefully)
			limiteVelHorizontal /= carefulWalkingSpeedDivisor;
		//Si el jugador está corriendo muy rápido, restringir la velocidad horizontal al máximo
		_myRb.velocity = new Vector2(Mathf.Clamp(_myRb.velocity.x,-limiteVelHorizontal,limiteVelHorizontal), _myRb.velocity.y);
		
		//Si el jugador está en el suelo, extender la ventana del coyote time
		if (isOnGround)
			_coyoteTime = Time.time + coyoteDuration;
	}

	void MidAirMovement()
	{
		//Si se presiona el botón jump Y el jugaor no se encuentra ya saltando Y YA SEA
		//que el jugador este en el suelo o dentro de la ventana de tiempo del coyote time...
		if (_input.jumpPressed && !isJumping && (isOnGround || _coyoteTime > Time.time) && !isHeadBlocked)
		{
			//...chequear si está agachado. Y de ser así...
			if (isCrouching)
			{
				//...levantarse y aplicar la fuerza extra
				StandUp();
				_myRb.AddForce(new Vector2(0f, crouchJumpBoost), ForceMode.Impulse);
			}
			//...El jugador está saltando...
			isJumping = true;

			//...registrar el tiempo en el que el jugador dejará de poder incrementar la fuerza el salto...
			_jumpTime = Time.time + jumpHoldDuration;

			//...agregar la fuerza del salto al rigidbody...
			_myRb.AddForce(new Vector2(0f, jumpForce), ForceMode.Impulse);

			//...aquí se debería ejecutar el sonido del salto
			//ejecutar sonido Salto
		}
		//de lo contrario, si se encuentra actualmente dentro de la ventana del salto...
		else if (isJumping)
		{
			//...y el boton de salto está sieno presionado, aplicar fuerza incremental al rigidbody...
			if (_input.jumpHeld)
				_myRb.AddForce(new Vector2(0f, jumpHoldForce), ForceMode.Impulse);

			//...y si el tiempo de salto se acabó, colocar el flag isJumping en false
			if (_jumpTime <= Time.time)
				isJumping = false;
		}

		//If player is falling to fast, reduce the Y velocity to the max
		if (_myRb.velocity.y < maxFallSpeed)
			_myRb.velocity = new Vector2(_myRb.velocity.x, maxFallSpeed);
		
		//Si está saltando y/o la velocidad vertical es mayor que un factor, entonces...
		if (isJumping || _myRb.velocity.y > 1f)
		{
			//... disminuir el ground distance a un factor pequeño, para detectar mejor cuando se deja de tocar el piso
			groundDistance = 0.1f;
		}
		else
		{
			//... de lo contrario, regresar el groundDistance al valor original
			groundDistance = _originalGroundDistance;
		}
	}

	private void RotateCharacter()
	{
		//Si el jugador está en el aire no permitir rotar
		if (!isOnGround)
			return;
		
		_turningSpeedFactor = Mathf.Sign(_input.horizontal);
		_facingDirection *= -1;
		//Si no está rotando seleccionar un eje al azar
		if (!isTurning)
		{
			_axis = _axes[Random.Range(0, 2)];
		}
		//Crear objetivo a Rotar e Iniciar la rutina de rotación del personaje
		Vector3 targetFacingRotation = new Vector3(_facingDirection, 0, 0).normalized;
		StartCoroutine(RotatePlayer(targetFacingRotation));
	}
    
	IEnumerator RotatePlayer(Vector3 target)
	{
		//mientras no se haya acercado lo suficiente al ángulo objetivo aplicar velocidad angular
		while (Vector3.Angle(transform.forward, target) >= 4f)
		{
			_myRb.constraints &= ~RigidbodyConstraints.FreezeRotationY;
			_myRb.angularVelocity = _axis * (turningSpeed * _turningSpeedFactor);
			isTurning = true;
			yield return null;
		}
		//Al acercarse lo suficiente al ángulo objetivo detener velocidad angular y ajustar rotación final
		_turningSpeedFactor = 0;
		_myRb.angularVelocity = Vector3.zero;
		transform.rotation = Quaternion.Euler(0,90*(target.x),0);
		_myRb.constraints |= RigidbodyConstraints.FreezeRotationY;
		isTurning = false;
	}

	void Crouch()
	{
		//The player is crouching
		isCrouching = true;
		//Apply the crouching collider size and offset
		_bodyCollider.height = _colliderCrouchHeight;
		_bodyCollider.center = _colliderCrouchCenter;
	}

	void StandUp()
	{
		//Si la cabeza del jugador está bloqueada, no se puede parar así que salimos de éste método
		if (isHeadBlocked)
			return;
		
		//El jugador no se encuentra agachado
		isCrouching = false;
		//Apply the standing collider size and offset
		_bodyCollider.height = _colliderStandHeight;
		_bodyCollider.center = _colliderStandCenter;
	}

	void WalkCarefully()
	{
		//El jugador esta caminando cuidadosamente
		isWalkingCarefully = true;
	}

	void StopWalkingCarefully()
	{
		//El jugador no esta caminando cuidadosamente
		isWalkingCarefully = false;
	}
	#region Metodos Raycast para detectar colisión con el GrounLayer
	//These two Raycast methods wrap the Physics2D.Raycast() and provide some extra
	//functionality
	private bool Raycast(Vector2 offset, Vector2 rayDirection, float length)
	{
		//Call the overloaded Raycast() method using the ground layermask and return 
		//the results
		return Raycast(offset, rayDirection, length, groundLayer);
	}

	private bool Raycast(Vector2 offset, Vector2 rayDirection, float length, LayerMask mask)
	{
		//Record the player's position
		Vector2 pos = transform.position;

		//Send out the desired raycast and record the result
		bool hit = Physics.Raycast(pos + offset, rayDirection, length, mask);

		//If we want to show debug raycasts in the scene...
		if (drawDebugRaycasts)
		{
			//...determine the color based on if the raycast hit...
			Color color = hit ? Color.red : Color.green;
			//...and draw the ray in the scene view
			Debug.DrawRay(pos + offset, rayDirection * length, color);
		}

		//Return the results of the raycast
		return hit;
	}
	#endregion
}
