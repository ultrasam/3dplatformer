# Proyecto para el canal de YouTube [_PadreGamer_](https://www.youtube.com/padregamer)
## 3D Platformer

Este repositorio contiene el proyecto de Unity del Video Juego 3D Platformer, que
se explica en el correspondiente Tutorial del canal de YouTube 
[_PadreGamer_](https://www.youtube.com/padregamer).

##### Los objetivos de este proyecto son:
1. Enseñar diferentes técnicas de utilizar animaciones en personajes 3D en [Unity](https://unity.com/).
2. Implementar animaciones provenientes del WebSite [Mixamo](https://www.mixamo.com/).
3. Utilizar un estilo LowPoly con assets creados por [Synty Studios](https://syntystore.com/).
4. Replicar mecánicas de juegos de plataforma estilo [Prince of Persia](https://en.wikipedia.org/wiki/Prince_of_Persia_(1989_video_game)) y [Ninja Gaiden](https://en.wikipedia.org/wiki/Ninja_Gaiden_(NES_video_game)).
5. Obtener ideas de la audiencia para implementar mecánicas creativas en el juego.

Esta es la primera iteración del videojuego 3DPlatformer para el canal
[*PadreGamer*](https://www.youtube.com/padregamer) por 
[Sócrates Medina](https://www.padregamer.com/).

Estado actual de Escena Principal:  
![Escena Video 0](https://sx2g5g.ch.files.1drv.com/y4m_XY7o7CPDycYkyMaCnDwRplUNkqcffj6Jre7KD2e5NM93PjCR-W-ER4mVWfoTc0Q6hGRSLQw2cn5ECJcQEQMn8d6ssHMTeHlL6_6Ity7f_leE1kKxcNdetyeUUcPXGKoeVjhTFEabzWiJuR7iCFryso-mYNnTwSg8uDpBMkWN33SsTyBT0yiejT_zN9bwBn5YiBfV08pxIKMhW2Gq6kpuA?width=1024&height=613&cropmode=none)